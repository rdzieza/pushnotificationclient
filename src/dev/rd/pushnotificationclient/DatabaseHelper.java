package dev.rd.pushnotificationclient;


import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper{
	private static Context context;
	private final static String NAME = "pushNotificationTestDB.db";
	private final static int VERSION = 1;
	private static DatabaseHelper instance;
	private static final String CREATE_MESSAGES = "CREATE TABLE IF NOT EXISTS MESSAGES(id INTEGER primary key autoincrement, message varchar(100), recieve_date varchar(30))";
	
	
	private DatabaseHelper(){
		super(context, NAME, null, VERSION);
	}
	
	/**
	 *Initialize method, used to initialize required context field to make self reference accessible. 
	 * @param context context with which database helper should be initialized
	 */
	public static void initialize(Context context) {
		DatabaseHelper.context = context;
		getConnection().onCreate(getConnection().getWritableDatabase());
	}
	
	/**
	 * Method allows you to obtain database handler reference and additionally it prevents existence of more than one database handler.
	 * @return database handler
	 */
	public static DatabaseHelper getConnection(){
		if(DatabaseHelper.instance == null){
			DatabaseHelper.instance = new DatabaseHelper();
		}
		return DatabaseHelper.instance;
	}
	
	/**
	 * Returns database cursor with all stored messages id, content and recieve date.
	 * @param db
	 * @return database cursor
	 */
	public static Cursor getMessages(SQLiteDatabase db){
		Cursor c = null;
		try{
			c = db.query("messages", new String[]{"message", "id as _id", "recieve_date"}, null, null, null, null, null);
		}catch(SQLException e){
			e.printStackTrace();
		}
		return c;
	}
	
	/**
	 * Adds one message to database.
	 * @param db database handler
	 * @param msg message content
	 * @param date recieve date
	 */
	public static void addMessage(SQLiteDatabase db, String msg, String date){
		db.execSQL("insert into messages (message, recieve_date) values ('" + msg + "','" + date +"')");
	}
	
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_MESSAGES);
//		db.execSQL("DROP TABLE MESSAGES");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
	
}
