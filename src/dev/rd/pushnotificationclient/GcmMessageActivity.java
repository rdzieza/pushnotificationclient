package dev.rd.pushnotificationclient;

import java.util.Date;
import java.util.Set;

import android.app.IntentService;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.gcm.GoogleCloudMessaging;


public class GcmMessageActivity extends IntentService {

	public GcmMessageActivity() {
		super("GcmMessageActivity");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
//		Log.v("t", "got the message");
		final Bundle bundle = intent.getExtras();
		
		Set<String> data = bundle.keySet();
		String message = "message recived!";
		///does recived message contains content marked with "message" key 
		if (data.contains("message")) {
			message = bundle.getString("message");
//			Log.v("t", "message: " + bundle.getString("message"));
			DatabaseHelper.initialize(this);
			//saves the message into database
			DatabaseHelper.addMessage(DatabaseHelper.getConnection().getWritableDatabase(), message, new Date().toString());	
		}
		//Create local notification
		Builder builder = new Builder(this);
		builder.setSmallIcon(R.drawable.logo);
		builder.setContentText(message);
		builder.setContentTitle("Notification recived");
		builder.setAutoCancel(true);
		//Specifies activity to be called
		Intent resultIntent = new Intent(this, MessagesListActivity.class);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		builder.setContentIntent(resultPendingIntent);
		stackBuilder.addParentStack(MessagesListActivity.class);
		Notification notification = builder.build();
		notification.defaults = Notification.DEFAULT_ALL;
		NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		manager.notify(2, notification);


	}

}
