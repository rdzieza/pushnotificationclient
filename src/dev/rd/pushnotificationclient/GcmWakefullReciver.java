package dev.rd.pushnotificationclient;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class GcmWakefullReciver extends WakefulBroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		//Specifies action to be taken when particular event occurs, called by
		//action intent filter <action android:name="com.google.android.c2dm.intent.RECEIVE" />
		ComponentName comp = new ComponentName(context.getPackageName(),
                GcmMessageActivity.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);

		
	}

}
