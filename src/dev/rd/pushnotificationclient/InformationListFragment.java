package dev.rd.pushnotificationclient;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Represents view showed to user after "information" option is chosen.
 * @author robert
 *
 */
public class InformationListFragment extends Fragment{
	private TextView idContent;
	private TextView ownerContent;
	private Button register;
	private Button unregister;
	
	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container, Bundle savedInstanceState) {
		View view = infl.inflate(R.layout.information_fragment_view, container, false);
		idContent = (TextView)view.findViewById(R.id.devicesIdContent);
		ownerContent = (TextView)view.findViewById(R.id.ownerNameContent);
		String ownerName = PreferenceHelper.readString("ownerName");
		String id = PreferenceHelper.readString("regID");
		idContent.setText(id);
		ownerContent.setText(ownerName);
		register = (Button)view.findViewById(R.id.register);
		unregister = (Button)view.findViewById(R.id.unregister);
		
		if(PreferenceHelper.readBoolean("isRegistered")) {
			register.setEnabled(false);
		}
		if(!PreferenceHelper.readBoolean("isRegistered")) {
			unregister.setEnabled(false);
		}
		return view;
	}
}
