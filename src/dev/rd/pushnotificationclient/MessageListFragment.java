package dev.rd.pushnotificationclient;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

public class MessageListFragment extends Fragment {
	private ListView messagesList;
	final String PROJECT_NUMBER = "1017112720995";
	

	@Override
	public View onCreateView(LayoutInflater infl, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = getActivity().getLayoutInflater().inflate(R.layout.message_list_fragment_view, null);
		messagesList = (ListView)view.findViewById(R.id.messagesList);
	
		TextView emptyMessage = new TextView(getActivity());
		emptyMessage.setText("You have no messages to display");
		emptyMessage.setPadding(20, 20, 20, 20);
		messagesList.setEmptyView(emptyMessage);
		messagesList.setDivider(null);
		messagesList.setDividerHeight(10);
		
		//Adapter creation, it connects data from database with matching views
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(getActivity(),
				R.layout.single_message_view,
				DatabaseHelper.getMessages(DatabaseHelper.getConnection()
						.getReadableDatabase()), new String[] { "message",
						"recieve_date" }, new int[] { R.id.messageContent,
						R.id.messageReciveDate },
				SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

		messagesList.setAdapter(adapter);
		return view;
	}

}
