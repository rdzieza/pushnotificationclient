package dev.rd.pushnotificationclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources.NotFoundException;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class MessagesListActivity extends Activity {
	//Google unique project ID, also refered as API key
	final String PROJECT_NUMBER = "1017112720995";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messages_list);
		DatabaseHelper.initialize(getApplicationContext());
		PreferenceHelper.initialize(getApplicationContext());
		
		
		//Checks which view should be loaded
		if (getIntent().getExtras() == null
				|| getIntent().getExtras().getInt("fragmentToAttach") == 0) {
			FragmentTransaction tr = getFragmentManager().beginTransaction();
			android.app.Fragment fr = new MessageListFragment();
			tr.replace(R.id.fragmentLayout, fr);
			tr.commit();
		} else if (getIntent().getExtras().getInt("fragmentToAttach") == 1) {
			FragmentTransaction tr = getFragmentManager().beginTransaction();
			android.app.Fragment fr = new InformationListFragment();
			tr.replace(R.id.fragmentLayout, fr);
			tr.commit();
		}

		// Dialog for users name
		if (PreferenceHelper.readBoolean("isFirstTime")
				|| !PreferenceHelper.readBoolean("isNotRegistering")) {
			Log.v("t", "firstRUN");
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Owner");

			final EditText inputName = new EditText(getApplicationContext());
			inputName.setTextColor(Color.GRAY);
			inputName.setHint(R.string.ownerHint);
			inputName.setSingleLine();

			builder.setPositiveButton("Register", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});

			builder.setView(inputName);
			final AlertDialog d = builder.create();
			d.show();

			Button button = d.getButton(AlertDialog.BUTTON_POSITIVE);
			button.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					if (inputName.getText().toString().equals("")) {
						Toast.makeText(getApplicationContext(),
								"You must provide your name", Toast.LENGTH_LONG)
								.show();
					} else {
						// PreferenceHelper.writeBoolean("isFirstTime", false);
						Log.v("t", "is not first time, at least anymore");
						PreferenceHelper.writeString("ownerName", inputName
								.getText().toString());
						Log.v("t", "first name is saved: "
								+ inputName.getText().toString());
						registerDevice(inputName.getText().toString());
						d.dismiss();

					}

				}
			});
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.messages_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			//Information option clicked, makes activity to load information fragment
			Intent intent = new Intent(this, MessagesListActivity.class);
			intent.putExtra("fragmentToAttach", 1);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Checks if there is existing internet connection
	 * @return true if there is internet conenction, false otherwise
	 */
	public boolean checkConnection() {
		ConnectivityManager cm = (ConnectivityManager) getApplicationContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null
				&& activeNetwork.isConnectedOrConnecting();
		// Log.v("t", "Connected: " + String.valueOf(isConnected));
		return isConnected;
	}
	
	/**
	 * Registers device with two services. The first step is to register device with GCM service, and obtain
	 * unique device ID and second is to register this device to 3rd application server together with proviced owners name
	 * @param name owner name under which device should be saved
	 */
	public void registerDevice(final String name) {
		PreferenceHelper.writeBoolean("isNotRegistering", false);
		if (!name.equals("")) {
			if (checkConnection()) {
				new AsyncTask<Void, Void, Void>() {

					GoogleCloudMessaging gcm = GoogleCloudMessaging
							.getInstance(getApplicationContext());

					@Override
					protected Void doInBackground(Void... parameters) {
						try {
							// register with GCM
							String regID = gcm.register(PROJECT_NUMBER);
							PreferenceHelper.writeString("regID", regID);
							Log.v("t", "gcm registered");
							// /
							HttpClient client = new DefaultHttpClient();
							HttpPost post = new HttpPost(getResources()
									.getString(R.string.registerUrl));
							List<NameValuePair> params = new ArrayList<NameValuePair>(
									3);
							// Log.v("t", "just before sending" + name);
							params.add(new BasicNameValuePair("id", regID));
							params.add(new BasicNameValuePair("owner", name));
							params.add(new BasicNameValuePair("os", "Android"));
							// Log.v("t", params.toString());
							post.setEntity(new UrlEncodedFormEntity(params));
							HttpResponse response = client.execute(post);
							BufferedReader br = new BufferedReader(
									new InputStreamReader(response.getEntity()
											.getContent()));
							String line = "";

							while ((line = br.readLine()) != null) {
								Log.v("t", line);
							}
							Log.v("t", "finished");
							PreferenceHelper.writeBoolean("isFirstTime", false);
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						super.onPostExecute(result);
						Log.v("t", "onPOsT");
						PreferenceHelper.writeBoolean("isNotRegistering", true);
						PreferenceHelper.writeBoolean("isRegistered", true);
					}
				}.execute(null, null, null);
			}
		}

	}

	/**
	 * Unregisters the user from 3rd application server database. 
	 * @param v reference to view which called this method, button by onclick event in this particular case
	 */
	public void unregister(View v) {
//		Log.v("t", "unregister");

		new AsyncTask<Void, Void, Void>() {
			String msg = "";
			String id;
			int code;

			@Override
			protected Void doInBackground(Void... arg0) {
				id = PreferenceHelper.readString("regID");

				if (!checkConnection()) {
					Log.v("t", "no intenet connection");
					msg = "you have no internet connection";
					this.cancel(true);
				}
				URL url = null;
				try {
					url = new URL(getResources().getString(R.string.removeUrl)
							+ "?id=" + id);
				} catch (MalformedURLException | NotFoundException e) {
					e.printStackTrace();
					msg = "url coding exception";
					this.cancel(true);
				}

				HttpURLConnection con;
				try {
					con = (HttpURLConnection) url.openConnection();
					con.setRequestMethod("DELETE");
					con.setRequestProperty("Content-Type",
							"application/x-www-form-urlencoded");
					con.setUseCaches(false);
					code = con.getResponseCode();
					Log.v("t", String.valueOf(con.getResponseCode()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					msg = "io exception";
					this.cancel(true);
				}
				return null;
			}

			@Override
			public void onPostExecute(Void result) {
				Log.v("t", "unregister, on post");
				if (!isCancelled()) {
					if (code == 200) {
						PreferenceHelper.writeString("regID", "missing");
						TextView idContent = (TextView) findViewById(R.id.devicesIdContent);
						if (idContent != null) {
							idContent.setText("missing");
							PreferenceHelper
									.writeBoolean("isRegistered", false);
							Button register = (Button) findViewById(R.id.register);
							Button unregister = (Button) findViewById(R.id.unregister);
							if (register != null) {
								register.setEnabled(true);
							}
							if (unregister != null) {
								unregister.setEnabled(false);
							}
						}
					}
				} else {
					Toast.makeText(getApplicationContext(), msg,
							Toast.LENGTH_LONG).show();
				}
			}

		}.execute(null, null, null);
	}
	
	/**
	 * Method used to re-register user in 3rd application server. Previously provided name is used.
	 * @param v view which called this method, button by onclick event in this particular case.
	 */
	public void register(View v) {
		new AsyncTask<Void, Void, Void>() {
			String msg = "";
			GoogleCloudMessaging gcm = GoogleCloudMessaging
					.getInstance(getApplicationContext());

			@Override
			protected Void doInBackground(Void... parameters) {
				if (checkConnection()) {
					try {
						// register with GCM
						String regID = gcm.register(PROJECT_NUMBER);
						PreferenceHelper.writeString("regID", regID);
						Log.v("t", "gcm registered");
						// /
						HttpClient client = new DefaultHttpClient();
						HttpPost post = new HttpPost(getResources().getString(
								R.string.registerUrl));
						List<NameValuePair> params = new ArrayList<NameValuePair>(
								3);
						String name = PreferenceHelper.readString("ownerName");
						params.add(new BasicNameValuePair("id", regID));
						params.add(new BasicNameValuePair("owner", name));
						params.add(new BasicNameValuePair("os", "Android"));
						Log.v("t", params.toString());
						post.setEntity(new UrlEncodedFormEntity(params));
						HttpResponse response = client.execute(post);
						PreferenceHelper.writeBoolean("isFirstTime", false);
					} catch (UnsupportedEncodingException e) {
						msg = "connection error";
						this.cancel(true);
						e.printStackTrace();
					} catch (IOException e) {
						msg = "connection error";
						this.cancel(true);
						e.printStackTrace();
					}
				} else {
					msg = "You hve no internet connection";
					this.cancel(true);
				}

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				Log.v("t", "onPOsT");
				if (!isCancelled()) {
					PreferenceHelper.writeBoolean("isNotRegistering", true);
					PreferenceHelper.writeBoolean("isRegistered", true);
					Button register = (Button) findViewById(R.id.register);
					Button unregister = (Button) findViewById(R.id.unregister);
					if (register != null) {
						register.setEnabled(false);
					}
					if (unregister != null) {
						unregister.setEnabled(true);
					}
					TextView idContent = (TextView) findViewById(R.id.devicesIdContent);
					if (idContent != null) {
						idContent.setText(PreferenceHelper.readString("regID"));
					}
				} else {
					Toast.makeText(getApplicationContext(), msg,
							Toast.LENGTH_LONG).show();
				}
			}
		}.execute(null, null, null);
	}

}
