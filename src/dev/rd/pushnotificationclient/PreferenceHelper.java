package dev.rd.pushnotificationclient;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;



public class PreferenceHelper {
	//Preferences file name
	private static final String NAME = "pushClientPrefs";
	//Selft reference
	private static SharedPreferences prefs;
	 
	/**
	 * Method used to obtain self reference
	 * @param context context to be used when getting shared preferences object
	 */
	public static void initialize(Context context){
		prefs = context.getSharedPreferences(NAME, 0);
	}
	
	/**
	 * Saves key-value pair storing String object
	 * @param key unique key of given object
	 * @param value value of stored object - type of String
	 */
	public static void writeString(String key, String value){
		Editor editor = prefs.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	/**
	 * Saves key-value pair storing boolean object
	 * @param key unique key of given object
	 * @param value boolean value of stored object
	 */
	public static void writeBoolean(String key, Boolean value){
		Editor editor = prefs.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	/**
	 * Finds and returns key-value String object with gives key
	 * @param key unique key of object to be found
	 * @return String value of object with given key or word "missing" if one doesn't exist
	 */
	public static String readString(String key){
		return prefs.getString(key, "missing");
	}
	
	/**
	 * Finds and returns key-value boolean object with given key
	 * @param key unique key of object to be found
	 * @return boolean value of object with given key or "true" if object does not exist
	 */
	public static boolean readBoolean(String key){
		return prefs.getBoolean(key, true);
	}
	
	
		
	
	
}
